package br.com.itau.clientesmicro.services;

import br.com.itau.clientesmicro.exceptions.CustomerNotFoundException;
import br.com.itau.clientesmicro.models.Customer;
import br.com.itau.clientesmicro.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public Customer create(Customer customer) {
        return customerRepository.save(customer);
    }

    public Customer getById(Long id) {
        Optional<Customer> byId = customerRepository.findById(id);

        if(!byId.isPresent()) {
            throw new CustomerNotFoundException();
        }

        return byId.get();
    }

}
