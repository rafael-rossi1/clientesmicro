package br.com.itau.clientesmicro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ClientesmicroApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientesmicroApplication.class, args);
	}

}
