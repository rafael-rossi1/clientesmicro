package br.com.itau.clientesmicro.repositories;

import br.com.itau.clientesmicro.models.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
}
